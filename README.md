# Netguru recruitment task: Movies Database API

This project is a simple REST API for a basic movie database interacting with external API for Netguru (full [instructions](instructions.md)).

## Installation

### Prerequisite

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Setup

Create a `.env` file and add your data (especially `OMDB_API_KEY` to get [here](https://www.omdbapi.com/apikey.aspx) and `MONGO_PASSWORD`).

```console
$ cp .env.example .env
```

Run the docker compose tool to build and start the database and the movies API service.

```console
$ docker-compose -d up
```

## Comments / Possible Improvements

1. Enforce more validation on input (e.g. when inserting a comment, ensure the movie ID is valid, limit the size of the comment, etc.)
1. Type and document the response objects (success and error) for the OAS
1. Add Swagger UI tool from generated OpenAPI specs (`swagger.json`) for better documentation
1. Authentication could be added to the API
1. Tests are provided as example of setup and should be extended to increase code coverage.
