FROM node:14.15.1

WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json .

RUN npm ci

EXPOSE ${PORT}

COPY . .

RUN npm run compile
