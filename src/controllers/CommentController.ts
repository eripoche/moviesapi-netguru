import {
  Controller,
  Route,
  Get,
  Body,
  Post,
  Query,
  Tags,
  SuccessResponse,
} from 'tsoa';

import { Comment } from '../models';
import { CommentService } from '../services';

type CommentPostParams = {
  /**
   * The ID of the movie the upsert related to the comment
   */
  movieId: string;
  /**
   * The content of the comment
   */
  content: string;
};

@Route('comments')
export class CommentController extends Controller {
  /**
   * Insert a comment in the database.
   */
  @Tags('Comments')
  @Post()
  public async postComment(
    @Body() { movieId, content }: CommentPostParams,
  ): Promise<Comment> {
    return CommentService.create({ movieId, content });
  }

  /**
   * Retrieves an array of comments related to a movie from the database.
   *
   * @param movieId The ID of the movie from which to get the comments
   *
   * @isInt offset offset should be a positive integer
   * @default offset 0
   * @minimum offset 0
   *
   * @isInt page page should be a positive integer
   * @default page 1
   * @minimum page 1
   *
   * @isInt limit limit should be a positive integer
   * @default limit 0
   * @minimum limit 10
   * @maximum limit 100
   */
  @Tags('Comments')
  @SuccessResponse(201, 'Created')
  @Get()
  public async getAll(
    @Query() movieId: string,
    @Query() offset = 0,
    @Query() page = 1,
    @Query() limit = 10,
  ): Promise<Comment[]> {
    return CommentService.get({ movieId, offset, page, limit });
  }
}
