import { OMDBService } from '../services';
import { Controller, Route, Get, Body, Post, Query, Tags } from 'tsoa';

import { Movie } from '../models';
import { MovieService } from '../services';

type MoviesPostParams = {
  /**
   * The title of the movie the upsert
   * @example "Batman"
   */
  title: string;
};

@Route('movies')
export class MovieController extends Controller {
  /**
   * Insert a movies in the database or updates its data.
   * The information is  fetched from the OMDB API.
   */
  @Tags('Movies')
  @Post()
  public async postMovie(
    @Body() { title }: MoviesPostParams,
  ): Promise<Movie | null> {
    return MovieService.upsert(await OMDBService.getByTitle(title));
  }

  /**
   * Retrieves an array of movies from the database.
   *
   * @isInt offset offset should be a positive integer
   * @default offset 0
   * @minimum offset 0
   *
   * @isInt page page should be a positive integer
   * @default page 1
   * @minimum page 1
   *
   * @isInt limit limit should be a positive integer
   * @default limit 0
   * @minimum limit 10
   * @maximum limit 100
   */
  @Tags('Movies')
  @Get()
  public async getAll(
    @Query() offset = 0,
    @Query() page = 1,
    @Query() limit = 10,
  ): Promise<Movie[]> {
    return MovieService.get({ offset, page, limit });
  }
}
