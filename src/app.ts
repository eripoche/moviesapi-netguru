import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as Router from '@koa/router';
import { ValidateError } from 'tsoa';

import { connect as connectDb } from './db';
import { RegisterRoutes } from './routes';

connectDb();

const router = new Router();
const server = new Koa();

server.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    if (err instanceof ValidateError) {
      ctx.status = 400;
      ctx.body = err.message;
    } else {
      ctx.status = err.code || 500;
      ctx.body = err.message;
    }
    ctx.app.emit('error', err, ctx);
  }
});

server.use(bodyParser());

RegisterRoutes(router);

server.use(router.routes()).use(router.allowedMethods());

const port = process.env.PORT || 8080;
server.listen(port, () =>
  console.log(`Server ready at http://localhost:${port}`),
);
