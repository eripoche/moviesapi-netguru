import { GetAllPaginationParams } from '.';
import { Movie, MovieModel } from '../models';

export class MovieService {
  public static async get(params: GetAllPaginationParams): Promise<Movie[]> {
    const { docs: movies } = await MovieModel.paginate(
      {},
      { ...params, select: { __v: 0 } },
    );
    return movies;
  }

  public static async upsert(movie: Partial<Movie>): Promise<Movie | null> {
    return MovieModel.findOneAndUpdate({ title: movie.title }, movie, {
      upsert: true,
      new: true,
      useFindAndModify: false,
      fields: { __v: 0 },
    });
  }
}
