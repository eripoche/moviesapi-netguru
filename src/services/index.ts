export * from './CommentService';
export * from './MovieService';
export * from './OMDBService';

export type GetAllPaginationParams = {
  offset?: number;
  page?: number;
  limit?: number;
};
