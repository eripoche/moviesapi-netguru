import { GetAllPaginationParams } from '.';
import { Comment, CommentModel } from '../models';

export type CommentGetAllParams = GetAllPaginationParams & { movieId: string };
export type CommentCreateParams = { movieId: string; content: string };

export class CommentService {
  public static async get({
    movieId,
    ...pagination
  }: CommentGetAllParams): Promise<Comment[]> {
    const { docs: comments } = await CommentModel.paginate(
      { movie: movieId },
      { ...pagination, select: { __v: 0 } },
    );
    return comments;
  }

  public static async create({
    movieId,
    content,
  }: CommentCreateParams): Promise<Comment> {
    return CommentModel.create({
      movie: movieId,
      content,
    });
  }
}
