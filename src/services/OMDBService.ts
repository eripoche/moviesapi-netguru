import Axios from 'axios';

import { NotFoundError } from '../errors';
import { Movie } from '../models';

type OMDBSearchParams = {
  apikey: string;
  type: 'movie' | 'serie' | 'episode';
  t: string;
};

type OMDBSearchResult = {
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Awards: string;
  Poster: string;
  Ratings: {
    Source: string;
    Value: string;
  }[];
  Metascore: string;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  Type: string;
  DVD?: string;
  BoxOffice: string;
  Production: string;
  Website: string;
  Response?: string;
};

export class OMDBService {
  private static BASE_URL =
    process.env.OMDB_API_URL || 'http://www.omdbapi.com';
  private static SEARCH_DEFAULT_PARAMS: Pick<
    OMDBSearchParams,
    'apikey' | 'type'
  > = {
    apikey: process.env.OMDB_API_KEY || '',
    type: 'movie',
  };

  public static async getByTitle(title: string): Promise<Movie> {
    const searchParams: OMDBSearchParams = {
      ...OMDBService.SEARCH_DEFAULT_PARAMS,
      t: title,
    };
    const { data }: { data: OMDBSearchResult } = await Axios.get(
      OMDBService.BASE_URL,
      {
        params: searchParams,
      },
    );
    if (data.Response === 'False') {
      throw new NotFoundError(`No movies found with title "${title}"`);
    }
    return OMDBService.searchResultToMovie(data);
  }

  private static searchResultToMovie(searchResult: OMDBSearchResult): Movie {
    delete searchResult['Response'];
    delete searchResult['DVD'];
    return Utils.lowerKeysFirstChar<Movie>(searchResult);
  }
}

class Utils {
  static lowerKeysFirstChar = <T>(o: {
    [k: string]: string | unknown[] | unknown;
  }): T =>
    Object.entries(o).reduce(
      (r, [k, v]) => ({
        ...r,
        [Utils.lowerFirstChar(k)]:
          v instanceof Array ? v.map((i) => Utils.lowerKeysFirstChar(i)) : v,
      }),
      <T>{},
    );

  static lowerFirstChar = (a: string): string =>
    a.charAt(0).toLowerCase() + a.slice(1);
}
