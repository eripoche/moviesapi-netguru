import { Document, model, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate';

/**
 * @tsoaModel
 * The Movie object contains common information across
 * every movie in the system.
 */
export interface Movie extends Document {
  /**
   * The title of the movie
   */
  title: string;
  year: string;
  rated: string;
  released: string;
  runtime: string;
  genre: string;
  director: string;
  writer: string;
  actors: string;
  plot: string;
  language: string;
  awards: string;
  poster: string;
  ratings: {
    source: string;
    value: string;
  }[];
  metascore: string;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  type: string;
  boxOffice: string;
  production: string;
  website: string;
}

const movieSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  year: String,
  rated: String,
  released: String,
  runtime: String,
  genre: String,
  director: String,
  writer: String,
  actors: String,
  plot: String,
  language: String,
  awards: String,
  poster: String,
  ratings: {
    type: [
      {
        source: String,
        value: String,
      },
    ],
    _id: false,
  },
  metascore: String,
  imdbRating: String,
  imdbVotes: String,
  imdbID: String,
  type: String,
  boxOffice: String,
  production: String,
  website: String,
});
movieSchema.plugin(mongoosePaginate);

export const MovieModel = model<Movie>('movie', movieSchema);
