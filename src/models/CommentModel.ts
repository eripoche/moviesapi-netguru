import { Document, model, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate';

import { Movie } from './MovieModel';

/**
 * @tsoaModel
 * The Comment object stores information about a comment made
 * about one movie.
 */
export interface Comment extends Document {
  /**
   * The movie that the comment is about
   */
  movie: Movie | string;
  /**
   * The content of the comment
   */
  content: string;
}

const commentSchema = new Schema({
  movie: {
    type: Schema.Types.ObjectId,
    ref: 'movie',
    required: true,
  },
  content: String,
});
commentSchema.plugin(mongoosePaginate);

export const CommentModel = model<Comment>('comment', commentSchema);
