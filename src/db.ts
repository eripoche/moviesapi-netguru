import * as mongoose from 'mongoose';

export async function connect(): Promise<void> {
  const {
    MONGO_HOST: host,
    MONGO_PORT: port,
    MONGO_USER: user,
    MONGO_PASSWORD: password,
    MONGO_DATABASE: database,
  } = process.env;
  await mongoose
    .connect(`mongodb://${user}:${password}@${host}:${port}/${database}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      authSource: 'admin',
    })
    .catch((err) => {
      console.log(err);
    });
}
