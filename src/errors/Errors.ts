export class ClientError extends Error {
  code = 400;
}

export class NotFoundError extends ClientError {
  code = 404;
}
