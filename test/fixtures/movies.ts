export const movie1 = {
  title: 'The Witcher 3: Wild Hunt - A Night to Remember',
  actors: 'Doug Cockle, Geneviève Doang, Laura Doddington, Jozsef Fodor',
  awards: '1 win.',
  boxOffice: 'N/A',
  director: 'István Zorkóczy',
  genre: 'Short, Action, Adventure, Mystery',
  imdbID: 'tt5091902',
  imdbRating: '9.9',
  imdbVotes: '3,097',
  language: 'English',
  metascore: 'N/A',
  plot:
    'Geralt rejoins his long-lost lover, Yennefer, in the town of White Orchard. Yennefer tells him that Emperor Emhyr has summoned him to the city of Vizima. Emhyr tasks Geralt with finding Ciri, who has recently been seen in several places.',
  poster:
    'https://m.media-amazon.com/images/M/MV5BNDU4YzJjMTAtMzBlOS00ZTBmLWEyNGYtZjM0NDkzNzg0ZDMwXkEyXkFqcGdeQXVyNTU0NDgwMzA@._V1_SX300.jpg',
  production: 'N/A',
  rated: 'N/A',
  ratings: [
    {
      source: 'Internet Movie Database',
      value: '9.9/10',
    },
  ],
  released: '15 May 2015',
  runtime: '4 min',
  type: 'movie',
  website: 'N/A',
  writer: 'N/A',
  year: '2015',
};

export const movie2 = {
  title: 'The Witcher',
  actors: 'Zachary Zurcher, Will Kirkwood, Elizabeth Williams, Luke Zurcher',
  awards: 'N/A',
  boxOffice: 'N/A',
  director: 'Zachary Zurcher',
  genre: 'Short, Fantasy',
  imdbID: 'tt7351402',
  imdbRating: '4.0',
  imdbVotes: '68',
  language: 'English',
  metascore: 'N/A',
  plot:
    'Famed monster hunter for hire, Geralt of Rivia faces impossible adversity during a violent time of war. Forced to put his monster contracts on hold, he must track down and uncover an evil even more unspeakable than he could ever imagine.',
  poster:
    'https://m.media-amazon.com/images/M/MV5BNDg3ZTE3MjItZTdhMy00ZDQzLTk1ZmItZDU4MWI0ODNlNDliXkEyXkFqcGdeQXVyMjExMDE1MzQ@._V1_SX300.jpg',
  production: 'N/A',
  rated: 'N/A',
  ratings: [
    {
      source: 'Internet Movie Database',
      value: '4.0/10',
    },
  ],
  released: '08 May 2017',
  runtime: '29 min',
  type: 'movie',
  website: 'N/A',
  writer:
    'Andrzej Sapkowski (based on characters created by), Zachary Zurcher (story)',
  year: '2017',
};

export const movie3 = {
  title: 'Batman',
  actors: 'Michael Keaton, Jack Nicholson, Kim Basinger, Robert Wuhl',
  awards: 'Won 1 Oscar. Another 7 wins & 26 nominations.',
  boxOffice: 'N/A',
  director: 'Tim Burton',
  genre: 'Action, Adventure',
  imdbID: 'tt0096895',
  imdbRating: '7.5',
  imdbVotes: '334,865',
  language: 'English, French, Spanish',
  metascore: '69',
  plot:
    'The Dark Knight of Gotham City begins his war on crime with his first major enemy being Jack Napier, a criminal who becomes the clownishly homicidal Joker.',
  poster:
    'https://m.media-amazon.com/images/M/MV5BMTYwNjAyODIyMF5BMl5BanBnXkFtZTYwNDMwMDk2._V1_SX300.jpg',
  production:
    'Warner Brothers, Guber-Peters Company, PolyGram Filmed Entertainment',
  rated: 'PG-13',
  ratings: [
    {
      source: 'Internet Movie Database',
      value: '7.5/10',
    },
    {
      source: 'Rotten Tomatoes',
      value: '71%',
    },
    {
      source: 'Metacritic',
      value: '69/100',
    },
  ],
  released: '23 Jun 1989',
  runtime: '126 min',
  type: 'movie',
  website: 'N/A',
  writer:
    'Bob Kane (Batman characters), Sam Hamm (story), Sam Hamm (screenplay), Warren Skaaren (screenplay)',
  year: '1989',
};

export const movies = [movie1, movie2];

export const omdbMovie1 = {
  Title: 'The Godfather',
  Year: '1972',
  Rated: 'R',
  Released: '24 Mar 1972',
  Runtime: '175 min',
  Genre: 'Crime, Drama',
  Director: 'Francis Ford Coppola',
  Writer:
    'Mario Puzo (screenplay by), Francis Ford Coppola (screenplay by), Mario Puzo (based on the novel by)',
  Actors: 'Marlon Brando, Al Pacino, James Caan, Richard S. Castellano',
  Plot:
    'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.',
  Language: 'English, Italian, Latin',
  Country: 'USA',
  Awards: 'Won 3 Oscars. Another 26 wins & 30 nominations.',
  Poster:
    'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg',
  Ratings: [
    { Source: 'Internet Movie Database', Value: '9.2/10' },
    { Source: 'Rotten Tomatoes', Value: '98%' },
    { Source: 'Metacritic', Value: '100/100' },
  ],
  Metascore: '100',
  imdbRating: '9.2',
  imdbVotes: '1,589,401',
  imdbID: 'tt0068646',
  Type: 'movie',
  DVD: 'N/A',
  BoxOffice: 'N/A',
  Production: 'Paramount Pictures',
  Website: 'N/A',
  Response: 'True',
};
