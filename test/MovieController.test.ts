import Axios from 'axios';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { MovieModel } from '../src/models';
import { MovieController } from '../src/controllers';
import { omdbMovie1 } from './fixtures/movies';

describe('MovieController', () => {
  let mc: MovieController;

  before(() => {
    mc = new MovieController();
  });

  describe('Post a movie', () => {
    before(() => {
      sinon.stub(Axios, 'get').resolves({ data: omdbMovie1 });
    });

    it('creates a movie in the database', async () => {
      let movie = await MovieModel.findOne({ title: 'The Godfather' });
      expect(movie).to.be.null;
      await mc.postMovie({ title: 'the godfather' });
      movie = await MovieModel.findOne({ title: 'The Godfather' });
      expect(movie).to.not.be.undefined;
    });
  });

  describe('Get movies', () => {
    it('creates a movie in the database', async () => {
      let movies = await MovieModel.find();
      expect(movies).to.have.lengthOf(3);
      movies = await mc.getAll();
      expect(movies).to.have.lengthOf(3);
    });
  });
});
