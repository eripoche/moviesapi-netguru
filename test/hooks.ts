import * as mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

import { CommentModel, MovieModel } from '../src/models';
import { movies as movieFixtures } from './fixtures/movies';

let mongoServer: MongoMemoryServer;

export const mochaHooks = {
  async beforeAll(): Promise<void> {
    console.log('MongoDB: Setting up in memory server');
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('MongoDB: Connected to', mongoUri);
    await initDb();
    console.log('MongoDB: Added fixtures');
  },
  async afterAll(): Promise<void> {
    console.log('MongoDB: Closing in memory server');
    await mongoose.disconnect();
    await mongoServer.stop();
    console.log('MongoDB: Disconnected');
  },
};

const initDb = async () => {
  const movies = await MovieModel.create(movieFixtures);
  await CommentModel.create([
    {
      movie: movies[0].id,
      content: 'Great movie',
    },
  ]);
};
